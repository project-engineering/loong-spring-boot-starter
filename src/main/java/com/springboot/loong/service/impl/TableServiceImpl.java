package com.springboot.loong.service.impl;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.write.metadata.WriteSheet;
import com.alibaba.fastjson2.JSONObject;
import com.springboot.loong.entity.SpecialCharInfo;
import com.springboot.loong.entity.TableEntity;
import com.springboot.loong.entity.TableStructure;
import com.springboot.loong.handler.CustomWidthStyleStrategy;
import com.springboot.loong.handler.EasyExcelCellWriteHandler;
import com.springboot.loong.handler.ExcelHyperlinkHandler;
import com.springboot.loong.mapper.TableMapper;
import com.springboot.loong.service.TableService;
import io.micrometer.common.util.StringUtils;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Service
public class TableServiceImpl implements TableService {
    @Resource
    private TableMapper tableMapper;

    /**
     * 导出数据库所有表结构信息
     * @param databaseName
     * @return
     */
    public void exportTableStructureToExcel(String databaseName) {
        Map<String, List<TableStructure>> map = getAllTableStructure(databaseName);
        // 创建ExcelWriter对象
        String filePath = "目标文件.xlsx";
        ExcelWriter excelWriter = EasyExcel.write(filePath).build();
        excelWriter = setFirstSheet(excelWriter,map);
        excelWriter = setOthersSheets(excelWriter,map);
        // 完成写入并关闭ExcelWriter
        excelWriter.finish();
    }



    /**
     * 获取所有表结构信息
     * @param databaseName
     * @return
     */
    private Map<String,List<TableStructure>> getAllTableStructure (String databaseName){
        List<TableEntity> tableList = tableMapper.getAllTableNames(databaseName);
        Map<String,List<TableStructure>> map = new HashMap<>();
        for (TableEntity table : tableList) {
            List<TableStructure> list = new ArrayList<>();
            List<Map<String, Object>> tableStructureList = tableMapper.getTableStructure(databaseName, table.getTableName());
            for (Map<String, Object> column : tableStructureList) {
                TableStructure tableStructure = TableStructure.builder().build();
                tableStructure.setOrdinalPosition(String.valueOf(column.get("ORDINAL_POSITION")));
                tableStructure.setColumnName((String) column.get("COLUMN_NAME"));
                tableStructure.setColumnType((String) column.get("COLUMN_TYPE"));
                if (column.get("IS_NULLABLE").equals("NO")) {
                    tableStructure.setIsNullable("√");
                } else {
                    tableStructure.setIsNullable("");
                }
                if (column.get("COLUMN_KEY").equals("PRI")) {
                    tableStructure.setColumnKey("√");
                } else {
                    tableStructure.setColumnKey("");
                }
                tableStructure.setColumnDefault((String) column.get("COLUMN_DEFAULT"));

                String columnComment = (String) column.get("COLUMN_COMMENT");
                // 拆分 COLUMN_COMMENT，假设格式为“字段描述:码值”
                if (StringUtils.isNotBlank(columnComment)) {
                    SpecialCharInfo specialCharInfo = findFirstSpecialChar(columnComment);
                    if (specialCharInfo != null) {
                        int position = specialCharInfo.getPosition();
                        tableStructure.setColumnComment(columnComment.substring(0,position));
                        String remark = columnComment.substring(position,columnComment.length());
                        // 替换结尾的特殊字符"!"为空格
                        tableStructure.setRemark(replaceSpecialCharacters(remark));
                    } else {
                        tableStructure.setColumnComment(columnComment);
                        tableStructure.setRemark("");
                    }
                } else {
                    tableStructure.setColumnComment("");
                    tableStructure.setRemark("");
                }
                list.add(tableStructure);
            }
            map.put(table.getTableName()+"-"+table.getTableComment(),list);
        }
        return map;
    }

    /**
     * 设置第一个sheet页
     * @param excelWriter
     * @param map
     * @return
     */
    private static ExcelWriter setFirstSheet(ExcelWriter excelWriter,Map<String, List<TableStructure>> map){
        // 第一次写入第一个sheet
        WriteSheet firstSheet = EasyExcel.writerSheet()
                .sheetNo(0)
                .sheetName("目录")
                .head(TableEntity.class)
                .registerWriteHandler(new ExcelHyperlinkHandler(1, new int[]{3}))//从第一行开始，对第3列的单元格添加超链接
                .registerWriteHandler(new CustomWidthStyleStrategy())
                .build();
        // 使用 Lambda 表达式和 Stream API 获取 A 和 B 并生成新的 List<Map>
        AtomicInteger i = new AtomicInteger(0);
        List<TableEntity> tableEntityList = map.keySet().stream()
                .map(s -> {
                    String[] arr = s.split("-");
                    TableEntity table = new TableEntity();
                    table.setSeq(i.addAndGet(1));
                    table.setTableName(arr[0]);
                    table.setTableComment(arr[1]);
                    table.setLink("Link");
                    return table;
                })
                .collect(Collectors.toList());
        return excelWriter.write(tableEntityList, firstSheet);
    }

    /**
     * 设置其他sheet页
     * @param excelWriter
     * @param map
     * @return
     */
    private static ExcelWriter setOthersSheets(ExcelWriter excelWriter,Map<String, List<TableStructure>> map){
        // 第二次写入多个sheet
        int sheetNo = 1;
        for (Map.Entry<String, List<TableStructure>> entry : map.entrySet()) {
            String[] tableNames = entry.getKey().split("-");
            JSONObject obj = new JSONObject();
            obj.put("tableName", tableNames[0]);
            obj.put("tableComment", tableNames[1]);
            EasyExcelCellWriteHandler easyExcelTitleHandler = new EasyExcelCellWriteHandler(obj);

            WriteSheet writeSheet = EasyExcel.writerSheet()
                    .sheetNo(sheetNo++)
                    .sheetName(tableNames[0])
                    .head(TableStructure.class)
                    .registerWriteHandler(easyExcelTitleHandler)
                    .registerWriteHandler(new ExcelHyperlinkHandler(0, new int[]{0}))//从第一行开始，对第3列的单元格添加超链接
                    .registerWriteHandler(new CustomWidthStyleStrategy())
                    .build();
            // 将数据写入Excel
            excelWriter.write(entry.getValue(), writeSheet);
        }
        return excelWriter;
    }

    public static SpecialCharInfo findFirstSpecialChar(String str) {
        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            if (!Character.isLetterOrDigit(c)) {
                return new SpecialCharInfo(c, i);
            }
        }
        // 表示未找到特殊字符
        return null;
    }

    private static String replaceSpecialCharacters(String str) {
        int start = 0;
        int end = str.length() - 1;

        // 找到需要替换的起始位置
        while (start < str.length() && !Character.isLetterOrDigit(str.charAt(start))) {
            start++;
        }

        // 找到需要替换的结束位置
        while (end >= 0 && !Character.isLetterOrDigit(str.charAt(end))) {
            end--;
        }

        // 替换特殊字符
        return str.substring(start, end + 1);
    }
}
