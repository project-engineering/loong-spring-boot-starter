package com.springboot.loong.service;

import com.springboot.loong.entity.TableStructure;
import java.util.List;
import java.util.Map;

public interface TableService {
    /**
     * 导出数据库所有表结构信息
     * @param databaseName
     * @return
     */
    void exportTableStructureToExcel(String databaseName);
}