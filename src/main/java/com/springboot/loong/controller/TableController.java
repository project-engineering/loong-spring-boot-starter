package com.springboot.loong.controller;

import com.springboot.loong.service.TableService;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.io.IOException;

@RestController
@RequestMapping("/table")
public class TableController {

    @Resource
    private TableService tableService;

    /**
     * 导出数据库所有表结构信息
     * @return
     */
    @GetMapping("/export")
    public String exportTableStructure() throws IOException {
        tableService.exportTableStructureToExcel("generator");
        return "Table structure exported to Excel successfully!";
    }
}