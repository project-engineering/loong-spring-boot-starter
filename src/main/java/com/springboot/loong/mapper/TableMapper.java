package com.springboot.loong.mapper;

import com.springboot.loong.entity.TableEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import java.util.List;
import java.util.Map;

@Mapper
public interface TableMapper {

    @Select("SELECT table_name,table_comment FROM information_schema.tables WHERE table_schema = #{databaseName}")
    List<TableEntity> getAllTableNames(@Param("databaseName")String databaseName);

    @Select("SELECT ordinal_position,column_name, data_type, column_type, is_nullable, column_key, column_default, extra, column_comment " +
            "FROM information_schema.columns " +
            "WHERE table_schema = #{databaseName} AND table_name = #{tableName} order by ordinal_position asc")
    List<Map<String, Object>> getTableStructure(@Param("databaseName")String databaseName, @Param("tableName")String tableName);

    @Select("SELECT table_comment " +
            "FROM information_schema.tables " +
            "WHERE table_schema = #{databaseName} AND table_name = #{tableName}")
    String getTableComment(@Param("databaseName")String databaseName, @Param("tableName")String tableName);
}