package com.springboot.loong.entity;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.*;
import com.alibaba.excel.enums.poi.BorderStyleEnum;
import com.alibaba.excel.enums.poi.FillPatternTypeEnum;
import com.alibaba.excel.enums.poi.HorizontalAlignmentEnum;
import com.alibaba.excel.enums.poi.VerticalAlignmentEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@ColumnWidth(25)
@ContentRowHeight(30)
@HeadRowHeight(30)
@Builder
@HeadStyle(fillPatternType = FillPatternTypeEnum.SOLID_FOREGROUND, fillForegroundColor = 71)
@HeadFontStyle(fontHeightInPoints = 12,color = 9)
@ContentFontStyle(fontHeightInPoints = 11)
@ContentStyle(horizontalAlignment = HorizontalAlignmentEnum.CENTER
        ,verticalAlignment = VerticalAlignmentEnum.CENTER
        ,borderLeft = BorderStyleEnum.MEDIUM
        ,borderRight = BorderStyleEnum.MEDIUM
        ,borderTop = BorderStyleEnum.MEDIUM
        ,borderBottom = BorderStyleEnum.MEDIUM)
public class TableEntity {
    @ExcelProperty(value={"数据库表目录","序号"},index = 0)
    @ContentStyle(horizontalAlignment = HorizontalAlignmentEnum.CENTER
            ,verticalAlignment = VerticalAlignmentEnum.CENTER
            ,borderLeft = BorderStyleEnum.MEDIUM
            ,borderRight = BorderStyleEnum.MEDIUM
            ,borderTop = BorderStyleEnum.MEDIUM
            ,borderBottom = BorderStyleEnum.MEDIUM)
    private Integer seq;

    @ExcelProperty(value={"数据库表目录","表名"},index = 1)
    @ContentStyle(horizontalAlignment = HorizontalAlignmentEnum.LEFT
            ,verticalAlignment = VerticalAlignmentEnum.CENTER
            ,borderLeft = BorderStyleEnum.MEDIUM
            ,borderRight = BorderStyleEnum.MEDIUM
            ,borderTop = BorderStyleEnum.MEDIUM
            ,borderBottom = BorderStyleEnum.MEDIUM)
    private String tableName;

    @ExcelProperty(value={"数据库表目录","中文描述"},index = 2)
    @ContentStyle(horizontalAlignment = HorizontalAlignmentEnum.LEFT
            ,verticalAlignment = VerticalAlignmentEnum.CENTER
            ,borderLeft = BorderStyleEnum.MEDIUM
            ,borderRight = BorderStyleEnum.MEDIUM
            ,borderTop = BorderStyleEnum.MEDIUM
            ,borderBottom = BorderStyleEnum.MEDIUM)
    private String tableComment;

    @ExcelProperty(value={"数据库表目录","超链接"},index = 3)
    @ContentStyle(horizontalAlignment = HorizontalAlignmentEnum.CENTER
            ,verticalAlignment = VerticalAlignmentEnum.CENTER
            ,borderLeft = BorderStyleEnum.MEDIUM
            ,borderRight = BorderStyleEnum.MEDIUM
            ,borderTop = BorderStyleEnum.MEDIUM
            ,borderBottom = BorderStyleEnum.MEDIUM)
    @ContentFontStyle(underline = 2,color = 60)
    private String link;
}
