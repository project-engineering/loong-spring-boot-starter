package com.springboot.loong.entity;

public class SpecialCharInfo {
    private char character;
    private int position;

    public SpecialCharInfo(char character, int position) {
        this.character = character;
        this.position = position;
    }

    public char getCharacter() {
        return character;
    }

    public int getPosition() {
        return position;
    }
}
