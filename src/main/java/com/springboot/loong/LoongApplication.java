package com.springboot.loong;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;

@SpringBootApplication
public class LoongApplication {
	public static void main(String[] args) {
		SpringApplication.run(LoongApplication.class, args);
	}
}
